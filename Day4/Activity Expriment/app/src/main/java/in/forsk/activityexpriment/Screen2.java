package in.forsk.activityexpriment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Screen2 extends AppCompatActivity {
    private final static String TAG = Screen2.class.getSimpleName();

    Button backBtn,openBrowserBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        NotificationUtils.notify(this, " onCreate");


        backBtn = (Button) findViewById(R.id.button3);
        openBrowserBtn = (Button) findViewById(R.id.button4);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        openBrowserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        NotificationUtils.notify(this,  " OnStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationUtils.notify(this,  " OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        NotificationUtils.notify(this,  " OnPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        NotificationUtils.notify(this,  " OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        NotificationUtils.notify(this,  " OnDestroy");
    }
}
